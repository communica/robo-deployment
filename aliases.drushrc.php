<?php

define('SITES', '/var/www');

$aliases = [];

$iterator = new RecursiveDirectoryIterator(SITES);

foreach ($iterator as $folder) {

  if (
      is_dir($folder->getPathname()) && 
      (
        (is_dir($folder->getPathname() . '/sites') && is_dir($folder->getPathname() . '/themes') && is_dir($folder->getPathname() . '/modules')) || 
        (is_dir($folder->getPathname() . '/drush') && is_dir($folder->getPathname() . '/vendor') && is_dir($folder->getPathname() . '/web'))
      )

    ) {

    $public_folder = is_dir($folder->getPathname() . '/web') ? '/web' : '';

    $aliases[$folder->getFileName()] = [
      'root' => $folder->getPathname() . $public_folder,
      'site_path' => $folder->getPathname() . $public_folder,
      'uri' => $folder->getFileName(),
    ];

  }

}

$iterator = new RecursiveDirectoryIterator(SITES);

foreach ($iterator as $folder) {

  if (is_dir($folder) AND is_dir($folder . '/sites') AND is_dir($folder . '/themes') AND is_dir($folder . '/modules')) {

    $sites_iterator = new RecursiveDirectoryIterator($folder . '/sites');

    foreach ($sites_iterator as $site) {

      if (is_dir($site) AND !in_array($site->getFileName(), ['all', 'default', '.', '..'])) {

        if (!array_key_exists($site->getFileName(), $aliases)) {

          $aliases[$site->getFileName()] = [
            'root' => $folder->getPathname(),
            'site_path' => $site->getPathname(),
            'uri' => $site->getFileName(),
          ];

        }

      }

    }

  }

}
