<?php

use Robo\Tasks;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends Tasks {

  protected $alias;

  protected $siteInfo;

  protected $siteComponents;

  public function runHelp() {
    $this->sayTemplate("robo --load-from ~/robo-deployment/ run:deploy @alias");
  }

  public function runSetupAliases($sitesPath = '/var/www') {

    $file = 'aliases.drushrc.php';

    $source = __DIR__ . '/' . $file;

    $destination = $_SERVER['HOME'] . '/.drush/' . $file;

    $this->sayTemplate('Create Drush aliases file in ' . $destination);

    $this->taskFilesystemStack()
      ->copy($source, $destination, true)
      ->run();

    $this->sayTemplate('Configure Drush aliases path');

    $this->taskReplaceInFile($destination)
      ->from('/var/www')
      ->to($sitesPath)
      ->run();
  }

  public function runClearCache($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->drushFlushCache();

    $this->flushOpcache();
  }

  public function runImportConfig($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->drushImportConfig();

    $this->flushOpcache();
  }

  public function runPartialDeploy($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->gitPull();

    $this->composerTasks();

    $this->drushUpdateDatabase();

    $this->drushFlushDrushCache();

    $this->flushOpcache();
  }

  public function runDeploy($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->gitPull();

    $this->composerTasks();

    $this->drushFlushCache();

    $this->drushUpdateDatabase();

    $this->drushImportConfig();

    $this->drushFlushDrushCache();

    $this->flushOpcache();
  }

  public function runTests($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->sayTemplate("Running tests");

    $this->taskPHPUnit()
      ->dir($this->drupalRoot() . '/core/')
      ->files($this->drupalRoot() . '/modules/custom/')
      ->debug()
      ->run();
  }

  public function runSyncDB($alias, $environment) {

    $sourceAlias = $alias . '.' . $environment;

    $target = $alias;

    $this->sayTemplate("Sync database {$sourceAlias} to {$alias}");

    $this->taskExec("drush sql-sync {$sourceAlias} {$target} -y")->run();
  }

  public function runSyncFiles($alias, $environment) {

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $localPath = $this->drupalRoot() . '/' . $this->drupalSetting('%paths')->{'%files'} . '/';

    $sourceAlias = $alias . '.' . $environment;

    $sourceSiteInfo = $this->taskExec("drush {$sourceAlias} status")
      ->arg('--format=json')
      ->run()->getMessage();

    $sourceSiteInfo = json_decode($sourceSiteInfo);

    $sourceDrupalRoot = $sourceSiteInfo->{'%paths'}->{'%root'};

    $sourceDrupalFiles = $sourceSiteInfo->{'%paths'}->{'%files'};

    $sourcePath  = $sourceDrupalRoot . '/' . $sourceDrupalFiles . '/';

    $this->sayTemplate("Downloading files");

    $this->taskRsync()
      ->fromPath($sourcePath)
      ->fromHost($environment . '.communica.nz')
      ->fromUser('ubuntu')
      ->toPath($localPath)
      ->recursive()
      ->times()
      ->compress()
      ->verbose()
      ->humanReadable()
      ->progress()
      ->exclude('advagg_css')
      ->exclude('advgg_js')
      ->exclude('css')
      ->exclude('ctools')
      ->exclude('js')
      ->exclude('imagecache')
      ->exclude('styles')
      ->exclude('tmp')
      ->exclude('php')
      ->run();

  }

  public function restartSupervisor($value = 'all') {
    $this->taskExec("sudo supervisorctl restart {$value}")->run();

    if ($value == 'all') {
      $this->taskExec("sudo supervisorctl status")->run();
    }
    else {
      $this->taskExec("sudo supervisorctl status {$value}")->run();
    }
  }

  public function updateSupervisor($value = 'all') {
    $this->taskExec("sudo supervisorctl update {$value}")->run();

    if ($value == 'all') {
      $this->taskExec("sudo supervisorctl status")->run();
    }
    else {
      $this->taskExec("sudo supervisorctl status {$value}")->run();
    }
  }

  protected function putSiteInMaintenance() {

    $this->sayTemplate('Put Drupal in maintenance mode');

    $this->drushVset('maintenance_mode', 1);
  }

  protected function duplicateSite() {

     $duplicatePath = sprintf('%s/../%s__%d', $this->drupalRoot(), $this->drupalSetting('uri'), $this->buildID);

     $this->taskFilesystemStack()
       ->mirror($this->drupalRoot(), $duplicatePath)
       ->run();

     $driver = $this->drupalSetting('db-driver');

     $user = $this->drupalSetting('db-username');

     $host = $this->drupalSetting('db-hostname');

     $dbName = $this->drupalSetting('db-name');

     $newDbName = sprintf('%s__%d', $dbName, $this->buildID);

     $dbDumpFile = "/tmp/{$newDbName}.sql";

     $this->taskExec("drush {$this->alias} sql-dump")
       ->arg('--gzip')
       ->arg("--result-file={$dbDumpFile}")
       ->run();


     $connection = new PDO("{$driver}:host={$host}", "{$user}", "root");

     $connection->exec("DROP DATABASE `{$dbName}__{$this->buildID}`;"); // to remove!! DEV only

     $connection->exec("CREATE DATABASE `{$dbName}__{$this->buildID}`;");

     $this->taskExec("gunzip < {$dbDumpFile}.gz | {$driver} -h {$host} -u {$user} -proot {$dbName}__{$this->buildID}")
       ->run();

     $this->taskFilesystemStack()->remove("{$dbDumpFile}.gz")->run();

     $settingsFile = sprintf('%s/%s', $duplicatePath, $this->drupalSetting('drupal-settings-file'));

     $this->taskReplaceInFile($settingsFile)
       ->from(["'database' => '{$dbName}'"])
       ->to(["'database' => '{$newDbName}'"])
       ->run();

   }

  protected function gitPull() {
    $this->taskGitStack()
    ->stopOnFail()
    ->dir($this->drupalRoot())
    ->pull()
    ->run();
  }

  public function runPullSubmodules($alias) {

    $this->stopOnFail(true);

    $this->alias = $alias;

    $this->drupalSiteInfo();

    $this->taskExec("git submodule foreach git pull")
      ->dir($this->drupalRoot())
      ->run();
  }

  protected function composerTasks() {

    if ($this->hasModule('composer_manager')) {

      $this->sayTemplate("Rebuilding composer.json file");

      $this->taskExec("drush {$this->alias} composer-rebuild")->run();
    }

    if (file_exists("{$this->drupalRoot()}/composer.json")) {

      $this->sayTemplate('Running composer install');

      $this->taskComposerInstall()
        ->dir($this->drupalRoot())
        ->run();

      $this->sayTemplate('Rebuilding class map');

      $this->taskComposerDumpAutoload()
        ->dir($this->drupalRoot())
        ->optimize()
        ->run();
    }

    if (file_exists("{$this->drupalRoot()}/../composer.json")) {

      $this->sayTemplate('Running composer install');

      $this->taskComposerInstall()
        ->dir("{$this->drupalRoot()}/../")
        ->run();

      $this->sayTemplate('Rebuilding class map');

      $this->taskComposerDumpAutoload()
        ->dir("{$this->drupalRoot()}/../")
        ->optimize()
        ->run();
    }

  }

   protected function revertAllFeatures() {

     $this->sayTemplate('Reverting all features');

     if ($this->hasModule('features')) {

       $this->taskExec("drush {$this->alias} fra")
         ->arg('-y')
         ->run();

     }
   }

  protected function drushUpdateDatabase(){

    $this->sayTemplate('Run all database updates');

    $this->taskExec("drush {$this->alias} updb")->arg('-y')->run();
  }

  protected function drushFlushCache() {

    $this->sayTemplate("Rebuilding Drupal's Cache");

    if ($this->drupalVersion() == 8) {
      $this->taskExec("drush {$this->alias} cr")->run();
    }

    if ($this->drupalVersion() == 7) {
      $this->taskExec("drush {$this->alias} rr")->run();
    }

  }

  protected function drushFlushDrushCache() {

    $this->sayTemplate("Flushing Drush Cache");

    $this->taskExec("drush {$this->alias} cc drush")->run();

  }

  protected function drushImportConfig() {

    if ($this->drupalVersion() == 8) {

      $this->sayTemplate("Import Drupal's Config");

      $this->taskExec("drush {$this->alias} cim -y")->run();
    }

  }

  protected function takeSiteOutOfMaintenance() {

    $this->sayTemplate('Taking Drupal out of maintenance mode');

    $this->drushVset('maintenance_mode', 0);
  }

  protected function drupalSiteInfo() {

    if (empty($this->siteInfo)) {
      $siteInfo = $this->taskExec("drush {$this->alias} status")
        ->arg('--format=json')
        ->run()->getMessage();

      $this->siteInfo = json_decode($siteInfo);
    }

    if (empty($this->siteComponents)) {
      $siteComponents = $this->taskExec("drush {$this->alias} pm-list")
        ->arg('--format=json')
        ->run()->getMessage();

      $this->siteComponents = json_decode($siteComponents);
    }

  }

  protected function isDrupalVersion($version) {
    return $version == $this->drupalVersion() ? TRUE : FALSE;
  }

  protected function drupalVersion() {
    return intval(substr($this->siteInfo->{'drupal-version'}, 0, 1));
  }

  protected function drushVset($name, $value){
    $this->taskExec("drush {$this->alias} vset {$name} {$value}")->run();
  }

  protected function hasModule($name) {

    if (isset($this->siteComponents->{$name}->status) AND $this->siteComponents->{$name}->status == 'enabled') {
      return TRUE;
    }

    return FALSE;
  }

  protected function drupalRoot() {
    return $this->drupalSetting('root');
  }

  protected function drupalSetting($name) {
    return $this->siteInfo->{$name};
  }

  protected function sayTemplate($text) {

    $lines = "=========================================";

    $this->say($lines);

    $this->yell($text);

    $this->say($lines);

  }

  protected function flushOpcache() {
    if (function_exists('opcache_reset')) {

      $this->sayTemplate("Reset opcache");

      opcache_reset();
    }
  }

}
